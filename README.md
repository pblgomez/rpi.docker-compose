# rpi.docker-compose

Docker compose for Raspberry Pi

## Install Raspbian
Use [Etcher](https://www.balena.io/etcher/) or dd to install the [Raspbian Image](https://downloads.raspberrypi.org/raspbian_lite_latest) to the SD card
```sh
dd bs=4M if=raspbian.img of=/dev/sdX conv=fsync status=progress
```
Change raspbian.img for rapsbian-<date>.img
!Important Change /dev/sdX for correct device /dev/... 


## Enable ssh
```sh
# mount the SD and create a ssh file at /boot
touch ssh
```

## Enable Wifi (optional)

Create wpa_supplicant.conf on the /boot directory.
Replace <COUNTRY CODE> with 2-Letter country: ej: US, UK, ES, IT
```sh
#wpa_supplicant.conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<COUNTRY CODE>

network={
    ssid="YOUR_SSID"
    scan_ssid=1
    psk="YOUR_PASSWORD"
    key_mgmt=WPA-PSK
}
```

## Boot your Raspberry Pi, default user: pi, default pass: raspberry

## Change passwd
```sh
sudo passwd pi
```

## Create another user for security
```
sudo adduser new_user
sudo usermod new_user -a -G pi,adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,spi,i2c,gpio
``` 

## Install git
sudo apt update && sudo apt install git tmux -y && git clone https://gitlab.com/pblgomez/rpi.docker-compose.git

## Change hostname and execute setup.sh
```sh
./rpi.docker-compose/setup.sh
```
